Notes on Dependancies:

Content Profile
---------------
We use this to allow us to store user profiles in nodes, rather than in the profile module which is part of Drupal core.  Drupal's core profile module does not store information in the database in an normalised fashion, and is generally thought of as being deprecated now - www.lullabot.com/audiocast/drupal_podcast_no_38_deprecated .  By keeping user profiles in nodes we afford ourselves the use of cck to define the fields, views to analyse and sort the users, and tools to easily import and export the data.

You need to install content profile module, set the profile type to be a content profile.  Then add a checkbox field with values 0 and 1 for newsletter subscription.


Soap Client
-----------
We are using this as a wrapper for the php5 soap API, though it also allows us to use the nuSOAP library in case the php extension is not available.

